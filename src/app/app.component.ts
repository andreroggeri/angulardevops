import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

interface Post {
  id: number;
  title: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  data: Post[] = [];
  title: string;

  constructor(private http: HttpClient) {
    this.title = 'Angular Devops Demo';
  }

  makeHttp() {
    const url = 'https://my-json-server.typicode.com/typicode/demo/posts';
    this.http.get(url).subscribe((response: Post[]) => {
      this.data = response;
    });
  }
}
