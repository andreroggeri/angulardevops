import { $, browser } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  loadPosts() {
    return $('#btn').click();
  }

  hasPosts() {
    return $('#postsContainer').isPresent() && $('#postsContainer').isDisplayed()
  }
}
