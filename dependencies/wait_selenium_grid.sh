#!/bin/bash
# wait_selenium_grid.sh

echo 'Waiting selenium hub to kick start ...'

while ! curl -s 'http://localhost:4444/wd/hub/status' \
| grep '\"ready\"' \
| grep "true" >/dev/null; do
  echo 'Waiting for the Grid'
  sleep 1
done &&

echo 'Application Started up, initiating tests'

